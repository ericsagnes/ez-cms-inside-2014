#CMSインサイド18 「eZ Publish」を使い込む

---

## ステップ1 - インストール

eZ Publishのインストール:

### Windowsでのインストール

必要なソフトウェア

- XAMPP環境 ( http://www.apachefriends.org/jp/ )
- gzip解凍ツール (お勧め: 7zip http://sevenzip.sourceforge.jp/ )
- テキストエディター (お勧め: gvim http://www.vim.org/download.php#pc )

### アーカイブをダウンロード

ダウンロードページ

http://share.ez.no/downloads/downloads/ez-publish-community-project-2012.8

直接ダウンロードリンク

http://share.ez.no/content/download/130661/708336/version/2/file/ezpublish_community_project-2012.8-with_ezc.tar.gz

Windowsではアーカイブを c:\xampp\cms-inside\ezpublish に解凍。  
Linux/Macでは/var/www/等に解凍。


### データベースの作成

Windowsの場合はXAMPPパネルからShellを起動  
LinuxとMacはターミナルを起動

    mysql -uroot -p
    create database cmsinside_ezp character set utf8;
    exit
    exit

### ホストの設定

ホストファイルを編集

- Windowsの場合 c:\Windows\System32\Drivers\etc\hosts
- Macの場合 /private/etc/hosts
- Linuxの場合 /etc/hosts

下記を追加

    127.0.0.1   ezpublish.cms-inside.local
    
### バーチャルホストの追加

#### Windowsの場合

C:\xampp\apache\conf\extra にレポジトリの ezpublish.cms-inside.local.conf をコピーします。

C:\xampp\apache\conf\httpd.conf に

    # Virtual hosts
    Include conf/extra/httpd-vhosts.conf

を

    # Virtual hosts
    Include conf/extra/httpd-vhosts.conf
    Include conf/extra/ezpublish.cms-inside.local.conf

#### Linux/Macの場合

Apacheのバーチャルホスト設定フォルダーに ezpublish.cms-inside.local.conf をコピーします。

httpd.conf に

Include 「ezpublish.cms-inside.local.confへの相対パス」

#### バーチャルホストファイルの修正

バーチャルホストファイルの「eZ Publishをインストールしたディレクトリ」をeZ Publishがインストールされたフォルダーのパスに置き換える。

### php.iniの設定

- Windowsの場合 C:\xampp\php\php.ini
- Macの場合 /private/etc/php.ini
- Linuxの場合 /etc/php/php.ini

memory_limitを128M以上に設定

    memory_limit=256M

max_execution_timeを300以上に設定

    max_execution_time=300

date.timezoneを設定します

    date.timezone = Asia/Tokyo

Apacheを再起動。

### WEBインストーラーを実行


ブラウザから http://ezpublish.cms-inside.local/  

サイト言語は日本語に設定。


パッケージは Website Interface (without demo content)  にします。

パッケージダウンロードがエラーを発生する場合、パッチを与える必要があります。

    patch -p1 < package1.patch
    patch -p1 < package2.patch

---

##ステップ2 - コンテンツクラスの作成

CMS-INSIDE管理サイトを作ります。

###CMS INSIDEフォルダーを作成

管理画面にログインし、「コンテンツツリー」に移動します。  
「新規作成」ボタンをクリックし、「Folder」をクリックし作成モードに入ります。  
「Name」を「CMS Inside」に設定し、「送信して公開」ボタンを押します。

###「CMS Inside」コンテンツクラスを作成

管理画面で、「システム設定」>「クラス」に移動します。  
「Content」クラスカテゴリをクリックし、「新規クラス」ボタンをクリックします。

属性は

* テキスト - タイトル - title
* XMLブロック - 紹介文 - presentation 
* 日付と日時 - 開催日時 - start_datetime

###「CMS Inside」インスタンスを作成します

管理画面で「コンテンツツリー」の「CMS Inside」フォルダーに移動します。  
「新規作成」ボタンをクリックし、「CMS Inside」をクリックし作成モードに入ります。
フィールドを入力します。

同様にコンテンツを２〜３個作ります。

###CMSフォルダーを作成

管理画面にログインし、「コンテンツツリー」に移動します。  
「新規作成」ボタンをクリックし、「Folder」をクリックし作成モードに入ります。  
「Name」を「CMS」に設定し、「送信して公開」ボタンを押します。

###「CMS」コンテンツクラスを作成

管理画面で、「システム設定」>「クラス」に移動します。  
「Content」クラスカテゴリをクリックし、「新規クラス」ボタンをクリックします。

属性は

* テキスト - 名前 - name
* テキスト - 言語 - language
* XMLブロック - 紹介文 - presentation 
* スターレーティング - 評価 - rating

###「CMS Inside」コンテンツクラスを修正

管理画面で、「システム設定」>「クラス」に移動します。  
「Content」クラスカテゴリをクリックし、「CMS Inside」をクリックし、「編集」ボタンをクリックします。

フィールドを追加します。

* 関連オブジェクト - CMS - cms

###「CMS」インスタンスを作成します

管理画面で「コンテンツツリー」の「CMS」フォルダーに移動します。  
「新規作成」ボタンをクリックし、「CMS」をクリックし作成モードに入ります。
フィールドを入力します。

同様にコンテンツを２〜３個作ります。

### CMS Insideインスタンスの編集

作ったCMS Insideインスタンスの編集し、CMS属性を設定します。

---

##ステップ3 - エクステンションの作成

###1.エクステンションフォルダーの作成

    mkdir -p extension/cmsinside/

###2.エクステンションを有効にする  
settings/override/site.ini.append.php の

    [ExtensionSettings]  
    ActiveExtensions[]  
    ActiveExtensions[]=ezmultiupload  

を下記のように変更する

    [ExtensionSettings]  
    ActiveExtensions[]  
    ActiveExtensions[]=cmsinside  
    ActiveExtensions[]=ezmultiupload  

管理画面の「システム設定」 > 「エクステンション」でcmsinsideが有効であるのを確認。

---

##ステップ4 - サイトアクセス

ベストプラクティスとして、できるだけエクステンションで完結させたいので、
サイトアクセスをcmsinsideエクステンションに作る

###1.サイトアクセスの作成

まずはサイトアクセスのフォルダーを作ります

    mkdir -p extension/cmsinside/settings/siteaccess/cmsisite

次は標準の設定ファイルをサイトアクセスにコピーします(frontはインストールの時に設定したサイトアクセス)

    cp settings/siteaccess/front/* extension/cmsinside/settings/siteaccess/cmsisite

ゴミファイルを削除する

    rm extension/cmsinside/settings/siteaccess/cmsisite/*~

###2.サイトアクセスの登録

settings/override/site.ini.append.phpに

    [SiteSettings]
    DefaultAccess=jpn
    SiteList[]
    SiteList[]=front
    SiteList[]=jpn
    SiteList[]=back

を下記のように変更します

    [SiteSettings]
    DefaultAccess=cmsisite
    SiteList[]
    SiteList[]=cmsinside
    SiteList[]=front
    SiteList[]=jpn
    SiteList[]=back

キャッシュを削除し:

    php bin/php/ezcache.php --clear-all

ページをリロードします。

「アクセスできません」は表示されます、原因は匿名ユーザは「cmsisite」サイトアクセス
を閲覧する権限がありません。

###3.権限の設定

管理画面の「ユーザ管理 > ロールとポリシー」にクリックし、Anonymousをクリックします。

権限の中に

    user 	login 	SiteAccess( front )  
    user 	login 	SiteAccess( jpn )

があります。どちらかを編集し、「cmsisite」サイトアクセスを追加し、ロールを保存します。

---

##ステップ3 - デザイン

###1.eZ Publishに開発設定を適用する

デザイン設定に向けて、キャッシュを無効にし、デバッグを有効にします。

settings/override/site.ini.append.phpの終わりに下記のブロックを追加:

    ####################
    # 開発設定
    ####################

    [ContentSettings]
    ViewCaching=disabled

    [DebugSettings]
    DebugOutput=enabled

    [TemplateSettings]
    Debug=enabled
    ShowXHTMLCode=disabled
    TemplateCompile=disabled
    TemplateCache=disabled
    ShowUsedTemplates=enabled

    [OverrideSettings]
    Cache=disabled

キャッシュを削除し:

    php bin/php/ezcache.php --clear-all

ページをリロードすると、ページの下にデバッグ情報が表示されます。

###2.デザインエクステンションの登録

cmsinsideエクステンションをデザインエクステンションとして登録:

extension/cmsinside/settings に design.ini.append.php を作成し、下記を追加します。

    <?php /* #?ini charset="utf-8"?

    [ExtensionSettings]
    DesignExtensions[]=cmsinside

    */ ?>

###3.フォルダーの準備

デザインフォルダーの追加:

    mkdir -p extension/cmsinside/design/cmsidesign/

デザインに必要なフォルダーを作ります:

- images: コンテンツでない画像
- stylesheets: cssファイル
- javascript: jsファイル
- templates: テンプレートファイル
- override: テンプレートオーバーライド

下記のコマンドで作ります:

    mkdir extension/cmsinside/design/cmsidesign/{images,override,stylesheets,templates,javascript}

###4.フォルダーの準備 - ベストプラクティス

オーバーライドを管理しやすくするために下記のように作ります

extension/cmsinside/design/cmsidesign/override/templates

- page ページ別のオーバーライド（node_id等）
- class クラス別のオーバーライド

下記のコマンドで作ります:

    mkdir -p extension/cmsinside/design/cmsidesign/override/templates/{page,class}

この構造の例:

    - page/
      - company.tpl
      - company/
        - about.tpl
    - class/
      - folder/
        - full.tpl
        - line.tpl
        - section/
          - sports/
            - full.tpl

標準テンプレートフォルダーは

- parts ヘッダー、フッターなどのincludeする各テンプレートパーツ
- node/view デフォルトのビューモード

下記のコマンドで作ります:

    mkdir -p extension/cmsinside/design/cmsidesign/templates/{node/view,parts}

この構造の例:

    - parts/
      - header.tpl
      - footer.tpl
    - node/
      - view/
        - full.tpl
        - line.tpl

###5.サイトアクセスにデザインを追加する

extension/cmsinside/settings/siteaccess/cmsisite/site.ini.append.phpの[DesignSettings]ブロックを下記から

    [DesignSettings]
    SiteDesign=ezwebin
    AdditionalSiteDesignList[]
    AdditionalSiteDesignList[]=base

下記に変更します

    [DesignSettings]
    SiteDesign=cmsidesign
    AdditionalSiteDesignList[]
    AdditionalSiteDesignList[]=ezwebin
    AdditionalSiteDesignList[]=base

---

##ステップ4 - テンプレートの追加

###1.レイアウトのカスタマイズ - pagelayout.tpl

標準オーバーライドであるため、かきのフォルダーに作ります。

    extension/cmsinside/design/cmsidesign/templates/pagelayout.tpl

pagelayout.tplで重要な変数は: 

- $module_result.content ビューの結果
- ezpagedata() 現ページのいろんなデータ（ezwebin必須）

スクラッチで書くのも良いが、慣れてない間はezwebin等のテンプレートをコピーし、
編集するのはお勧め。

ナビゲーション、ヘッダー、フッターなどはパーツ化して、{include}で呼び出す。

ナビゲーションの場合:

    {include uri='design:parts/navigation.tpl'}

テンプレートは下記のように置きます

    extension/cmsinside/design/cmsidesign/templates/parts/navigation.tpl

デザインにあるcssとjsは{ezdesign}を使って読み込みます:

    <link rel="stylesheet" href={'stylesheets/style.css'|ezdesign} type="text/css" media="screen" />  
    <script charset="utf-8" src={'javascript/script.js'|ezdesign} type="text/javascript"></script>


###2.ノードテンプレート

管理画面を使って「folder」クラスのオブジェクトを作ります。

folderのデフォルト表示(full)のテンプレートを作ります。

テンプレートが利用されるためにオーバーライド設定を追加します。

    extension/cmsinside/settings/siteaccess/cmsinside/override.ini.append.php

の上に下記のオーバーライドブロックを追加する

    [folder_full]
    Source=node/view/full.tpl
    MatchFile=class/folder/full.tpl
    Subdir=templates
    Match[class_identifier]=folder

テンプレートを下記のパスに置きます。

    extension/cmsinside/design/cmsidesign/override/templates/class/folder/full.tpl

ノードテンプレートでは**$node**変数が利用できます。  
**$node**は現在のノードの情報を細かく持っています:

- $node.name ノードの名前
- $node.url_alias ノードのURL
- $node.data_map ノードの属性

ノードテンプレートにノードの属性を表示するには attribute_view_gui が使えます。例：

    {attribute_view_gui attribute=$node.data_map.description}

他のノードもフェッチできます（全テンプレート共通）。例:

    {def $items = fetch('content','list', hash(
        'parent_node_id', 2,
        'limit', 5
    ))}

---

##ステップ5 - 完成に向けて - ベストプラクティス

管理画面で必要なコンテンツクラスを作って、それらのテンプレートを追加する。

gitで各エクステンションを管理して、メインエクステンションにdataと言うフォルダーを作り、そこに定期的にデータベースのダンプをとり、コミットする（開発段階）:

    mysqldump -uユーザ名 -p --add-drop-table データベース名 > extension/cmsinside/data/db.sql

必要に応じて、エクステンションに新しいテンプレートオペーレーター、モジュール、ワークフローイベントなどを追加する。

/kernel、/lib、/design等のデフォルトフォルダーはできるだけ変更しないで、エクステンションで完結させる。

メインエクステンションで再利用で汎用な機能やテンプレートは別なエクステンションにする。

{node_view_gui}、{attribute_view_gui}と{include}を活用して、2回同じテンプレートロジックを書かないようにします。

複雑なテンプレートロジックはテンプレートオペーレーターでまとめましょう。

変なエラーが出れば、まずはキャッシュを削除する:

    php bin/php/ezcache.php --clear-all

[リファレンス](http://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/)と[API](http://pubsvn.ez.no/doxygen/trunk/html/)を活用する。




















